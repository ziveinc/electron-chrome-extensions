export declare const addExtensionListener: (extensionId: string, name: string, callback: Function) => void;
export declare const removeExtensionListener: (extensionId: string, name: string, callback: any) => void;
export declare const hasExtensionListener: (extensionId: string, name: string, callback: Function) => boolean;
